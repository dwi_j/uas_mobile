import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyProfil extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.lightBlueAccent,
      appBar: new AppBar(
          backgroundColor: Colors.lightBlueAccent,
          title: new Center(
            child: new Text("Profil"),
          )),
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(100.0),
                child: Image.network(
                  'https://i.ibb.co/g3f657t/trio-ngepet.png',
                  width: 150.0,
                  height: 150.0,
                  fit: BoxFit.cover,
                ),
              ),
              Text(
                "PTI 4A",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20.0,
                  height: 2.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                "Ni Kadek Dwi Juniantari",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 15.0,
                  height: 1.0,
                ),
              ),
              Text(
                "Anak Agung Ayu Suwandewi",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 15.0,
                  height: 1.0,
                ),
              ),
              Text(
                "Putu Widya Pratiwi",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 15.0,
                  height: 1.0,
                ),
              ),
              Card(
                margin: EdgeInsets.only(top: 40.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Card(
                          color: Colors.lightBlueAccent,
                          margin: EdgeInsets.only(left: 10.0, right: 10.0),
                          child: Column(
                            children: <Widget>[
                              Icon(
                                Icons.school,
                                size: 110,
                                color: Colors.green,
                              ),
                              Text(
                                'Undiksha',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17.0,
                                  height: 2.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              )
                            ],
                          )),
                    ),
                    Expanded(
                      child: Card(
                          color: Colors.lightBlueAccent,
                          margin: EdgeInsets.only(
                            left: 10.0,
                            right: 10.0,
                            top: 10.0,
                            bottom: 10.0,
                          ),
                          child: Column(
                            children: <Widget>[
                              Icon(
                                Icons.group,
                                size: 110,
                                color: Colors.yellow,
                              ),
                              Text(
                                'Trio Ngepet',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17.0,
                                  height: 2.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              )
                            ],
                          )),
                    )
                  ],
                ),
              ),
              Card(
                margin: EdgeInsets.only(top: 10.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Card(
                          color: Colors.lightBlueAccent,
                          margin: EdgeInsets.only(left: 10.0, right: 10.0),
                          child: Column(
                            children: <Widget>[
                              Icon(
                                Icons.favorite,
                                size: 110,
                                color: Colors.purple,
                              ),
                              Text(
                                'Paduan Suara',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17.0,
                                  height: 2.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              )
                            ],
                          )),
                    ),
                    Expanded(
                      child: Card(
                          color: Colors.lightBlueAccent,
                          margin: EdgeInsets.only(
                            left: 10.0,
                            right: 10.0,
                            top: 10.0,
                            bottom: 10.0,
                          ),
                          child: Column(
                            children: <Widget>[
                              Icon(
                                Icons.movie,
                                size: 110,
                                color: Colors.blueAccent,
                              ),
                              Text(
                                'Horor & Mistery',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17.0,
                                  height: 2.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              )
                            ],
                          )),
                    )
                  ],
                ),
              ),
            ]),
      ),
    );
  }
}
